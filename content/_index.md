---
title:      "Dr Eliyahu Goldratt: Twórca teorii ograniczeń"
date:       2019-03-31
linktitle:  "Dr Eliyahu Goldratt"
menu:       "main"
weight:     1
draft:      false
---

<div class="hero-img">
  <img src="/goldratt.jpg" alt="Zdjęcie Dr Eliyahu Goldratt" />
  <figcaption>Dr Eliyahu Goldratt, twórca teorii ograniczeń</figcaption>
</div>

# Dr Eliyahu Goldratt<br/>Twórca teorii ograniczeń

<div class="col-6">
{{< markdown >}}
Dr Eliyahu M. Goldratt (1948-2011) był izraelskim fizykiem, który zastosował podejście naukowe do rozwiązywania problemów na co dzień trapiących firmy, organizacje nienastawione na zysk oraz zwykłych ludzi.

Goldratt jest określany mianem „geniusza” (*Business Week*), „guru biznesu” (magazyn *Fortune*), „rzeźnika świętych krów” (anonimowe), czy „Einsteina biznesu” ([Marek Kowalczyk](https://marekkowalczyk.pl/)). 

Był doskonałym mówcą i autorem licznych bestsellerów. 
Jego ulubioną formą był stworzony przez niego gatunek, tj. powieść biznesowa. 
Przede wszystkim był jednak liderem biznesu, który niezliczonej liczbie firm pomógł zwielokrotnić zyski.

Koncepcja, którą w latach 70-tych XX. wieku stworzył Goldratt nazywa się teorią ograniczeń (*theory of constraints*, TOC). 
Jest ona opisana m.in. w światowym bestsellerze [„Cel I: Doskonałość w produkcji”](http://mintbooks.pl/cel1) --- ponad cztery miliony sprzedanych egzemplarzy.

Jeff Bezos, najbogatszy człowiek świata i twórca Amazon.com, umieścił „Cel I” na liście 12 książek, które ukształtowały jego myślenie o biznesie --- jak donosi MSNBC.

Dziś teorię ograniczeń wykłada się na wielu renomowanych uczelniach, a w Polsce m.in. w [Wyższej Szkole Bankowej we Wrocławiu](https://mndrn.co/studia-zarzadzenie-projektem)
{{</ markdown >}}
</div>

<div class="col-6">
{{< markdown >}}
Zastosowania teorii ograniczeń obejmują m.in.:

|  Dziedzina biznesu                    |          Książka                                                                                    |
| ------------------------------------: | :-------------------------------------------------------------------------------------------------- |
| zarządzanie produkcją                 | [„Cel I: Doskonałość w produkcji”](http://mintbooks.pl/cel1)                                        |
| zarządzanie projektami                | [„Łańcuch krytyczny: Projekty na czas”](http://mintbooks.pl/lancuch)                                |
| podejmowanie decyzji finansowych      | [„Finanse do góry nogami: Zdroworozsądkowa rewolucja w rachunkowości”](http://mintbooks.pl/finanse) |
| rozwiązywanie problemów biznesowych   | [„Cel 2: To nie przypadek”](http://mintbooks.pl/cel2)                                               |
| strategia firmy                       | [„Cel 2: To nie przypadek”](http://mintbooks.pl/cel2)                                               |
| dystrybucja i logistyka               | [„Czy to nie oczywiste?! Doskonałość w dystrybucji”](http://mintbooks.pl/czytonieoczywiste)         |

Goldratt był twórcą *Goldratt Consulting* oraz *TOC for Education*, organizacji non-profit, zajmującej się zapoznawaniem nauczycieli i uczniów z narzędziami myślowymi teorii ograniczeń.

W grudniu 2006, dr Goldratt po raz pierwszy odwiedził Polskę, kraj swoich przodków. 
Podczas jednodniowego warsztatu w Warszawie przedstawił koncepcję *Viable Vision*, czyli jak zwiększyć zysk firmy do poziomu uznawanego za niemożliwy do osiągnięcia. 
Organizatorami wizyty byli: Puls Biznesu oraz [MANDARINE Project Partners](http://mandarine.co/).

{{</ markdown >}}
</div>
