---
title:  "Książki Goldratta"
date:   2018-12-04
linktitle: "Książki"
menu: "main"
draft: false
---

## Pozycje, które ukazały się w Polsce

<div class="container" margin="auto">

  <a href="http://mintbooks.pl/cel1"              ><img alt="Goldratt, „Cel I: Doskonałość w produkcji”"                    class="img-responsive"  src="/goldratt-cel-1.png"                  /></a>
  <a href="http://mintbooks.pl/cel2"              ><img alt="Goldratt, „Cel II: To nie przypadek”"                          class="img-responsive"  src="/goldratt-cel-2.png"                  /></a>
  <a href="http://mintbooks.pl/lancuch"           ><img alt="Goldratt, „Łańcuch krytyczny: Projekty na czas”"               class="img-responsive"  src="/goldratt-lancuch-krytyczny.png"      /></a>
  <a href="http://mintbooks.pl/wolnosc"           ><img alt="Goldratt, „Goldratt, Wolność wyboru”"                          class="img-responsive"  src="/goldratt-wolnosc-wyboru.png"         /></a>
  <a href="http://mintbooks.pl/czytonieoczywiste" ><img alt="Goldratt, „Czy to nie oczywiste?! Doskonałość w dystrubucji”"  class="img-responsive"  src="/goldratt-czy-to-nie-oczywiste.png"   /></a>

</div>

<p>Polskim wydawcą książek Goldratta o teorii ograniczeń jest [MINT Books](http://mintbooks.pl/). 

