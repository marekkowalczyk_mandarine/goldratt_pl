---
title: "Zasady teorii ograniczeń"
linktitle: "Zasady teorii ograniczeń"
date: 2018-11-28T15:06:20+02:00
menu: "main"
weight: 2
draft: false
---

<div class="hero-img">
  <img src="/goldratt_grave.jpg" alt="Ostateczne miejsce spoczynku Dra Eliyahu Goldratta" />
  <figcaption>Nagrobek dra Eliyahu Goldratta z wyrytymi zasadami teorii ograniczeń<figcaption/>
</div>

1. Ludzie są dobrzy.
2. Każdy konflikt można usunąć.
3. Każda sytuacja — niezależnie od tego, jak wydaje się początkowo złożona — jest niezwykle prosta.
4. Każdą sytuację można znacząco poprawić; nawet niebo nie jest kresem możliwości.
5. Każdy może żyć pełnią życia.
6. Zawsze jest rozwiązanie win-win.

Aby lepiej poznać teorię ograniczeń, zobacz [powieści biznesowe Goldratta](https://www.goldratt.pl/pages/ksiazki/).

Aby zastosować teorię ograniczeń do swojej sytuacji, skontaktuj się z [MANDARINE Project Partners](https://mandarine.co/kontakt/).
