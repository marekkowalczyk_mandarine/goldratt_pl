---
title: "Cytaty"
date: 2018-12-04
linktitle: "Cytaty"
menu: "main"
draft: false
---

<!-- Using HTML blockquote here else the content of the blockquote is wrapped in a paragraph tag by Hugo -->

<blockquote class="intro-quote">Kluczem do myślenia jak prawdziwy naukowiec jest uznanie, że każda rzeczywista sytuacja niezależnie od tego, jak początkowo wydaje się złożona — jest w gruncie rzeczy, gdy zostanie już zrozumiana, żenująco prosta.</blockquote>

<div class="flex-container">
<blockquote>Ludzie są dobrzy (i nie są głupi).</blockquote>

<blockquote>Każdy konflikt można usunąć.</blockquote>

<blockquote>Powiedz mi, jak mnie mierzysz, a powiem Ci, jak będę się zachowywał.</blockquote>

<blockquote>Lepiej jest mieć mniej więcej rację, niż być dokładnie w błędzie.</blockquote>

<blockquote>Łańcuch jest tak silny, jak jego najsłabsze ogniwo.</blockquote>

<blockquote>Pomysły to nie rozwiązania.</blockquote>
</div>
