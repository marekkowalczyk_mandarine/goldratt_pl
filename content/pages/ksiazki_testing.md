---
title:  "Książki Goldratta, które ukazały się w Polsce"
date:   2018-12-04
draft: false
---

<div class="container">

  <a href="http://mintbooks.pl/cel1"              ><img alt="Goldratt, „Cel I: Doskonałość w produkcji”"                    class="img-responsive"  src="/goldratt-cel-1.png"                  /></a>
  <a href="http://mintbooks.pl/cel2"              ><img alt="Goldratt, „Cel II: To nie przypadek”"                          class="img-responsive"  src="/goldratt-cel-2.png"                  /></a>
  <a href="http://mintbooks.pl/lancuch"           ><img alt="Goldratt, „Łańcuch krytyczny: Projekty na czas”"               class="img-responsive"  src="/goldratt-lancuch-krytyczny.png"      /></a>
  <a href="http://mintbooks.pl/wolnosc"           ><img alt="Goldratt, „Goldratt, Wolność wyboru”"                          class="img-responsive"  src="/goldratt-wolnosc-wyboru.png"         /></a>
  <a href="http://mintbooks.pl/czytonieoczywiste" ><img alt="Goldratt, „Czy to nie oczywiste?! Doskonałość w dystrubucji”"  class="img-responsive"  src="/goldratt-czy-to-nie-oczywiste.png"   /></a>

</div>

<p>Polskim wydawcą książek Goldratta o teorii ograniczeń jest [MINT Books](http://mintbooks.pl/). 

<div class="row">
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="http://mintbooks.pl/cel1">
        <img src="/goldratt-cel-1.png" alt="Lights" style="width:20%">
        <div class="caption">
          <p>
          Wjeżdżając dziś o siódmej trzydzieści rano na teren zakładu zobaczyłem go na parkingu. 
          Znany mi dobrze czerwony mercedes zaparkowany był obok hali produkcyjnej, przy pomieszczeniach biurowych. 
          Stał na moim miejscu parkingowym! 
          Któż inny, jak nie Bill Peach, zrobiłby coś takiego? 
          Nieważne, że cały parking jest o tej godzinie pusty i że tuż obok są stanowiska oznaczone „Goście”. 
          Nie, Bill musi parkować na miejscu przeznaczonym właśnie dla mnie. 
          On lubi robić takie subtelne aluzje. 
          No więc dobrze, jest wiceprezesem odpowiedzialnym za całą dywizję w naszym holdingu, a ja tylko dyrektorem jednej z fabryk. 
          Może parkować swego przeklętego mercedesa, gdzie tylko zechce.
          </p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="/w3images/nature.jpg">
        <img src="/w3images/nature.jpg" alt="Nature" style="width:100%">
        <div class="caption">
          <p>Lorem ipsum...</p>
        </div>
      </a>
    </div>
  </div>
  <div class="col-md-4">
    <div class="thumbnail">
      <a href="/w3images/fjords.jpg">
        <img src="/w3images/fjords.jpg" alt="Fjords" style="width:100%">
        <div class="caption">
          <p>Lorem ipsum...</p>
        </div>
      </a>
    </div>
  </div>
</div>
